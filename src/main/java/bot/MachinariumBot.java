package bot;

import connection.Database;
import connection.InformationDataUser;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 * Created by Vitali on 24.10.2017.
 */
public class MachinariumBot extends TelegramLongPollingBot {

    //private ConnectionDatabase connectionMysql;
    private Database database;

    public MachinariumBot() {

    }

    // Полученное обновление
    public void onUpdateReceived(Update update) {
        incomingUpdate(update);
    }

    public String getBotUsername() {
        return "MachinariumSuperBot";
    }

    public String getBotToken() {
        return "479400190:AAFSoZGbcaNrvtNLUCnWJfJsGRQRqDlVm-I";
    }

    // Checking incoming update
    private void incomingUpdate(Update update) {
        InformationDataUser infDb = new InformationDataUser(update);
        database = new Database();
        if (update.hasMessage())
        {
            if (database.checkUserInDatabase(update))
            {
                if (update.getMessage().hasText())
                {
                    System.out.println("TEXT");
                    if (update.getMessage().getText().equals("/reg"))
                    {
                        String messageText = "Мой милый Друг, ты уже зарегистрирован в системе! Для вызова списка команд, отправь мне команду /help";
                        database.saveTextSentByUserInDatabase(update, "/reg");
                        database.saveTextSentToUser(update, messageText);
                        sendServiceMessageToUser(update, messageText);
                    }
                    else if (update.getMessage().getText().equals("/help"))
                    {
                        System.out.println("СПИСОК КОМАНД");
                        database.saveTextSentByUserInDatabase(update, "/help");
                    }
                    else
                    {
                        database.saveTextSentByUserInDatabase(update, update.getMessage().getText());
                        database.saveTextSentToUser(update, update.getMessage().getText());
                        sendServiceMessageToUser(update, update.getMessage().getText());
                    }
                }
                else if (update.getMessage().hasPhoto())
                {
                    System.out.println("PHOTO");
                    database.savePhotoSentByUserInDatabase(update);
                }
                else if (update.getMessage().hasDocument())
                {
                    System.out.println("DOCUMENT");
                    database.saveDocumentSentByUserInDatabase(update);
                }
                else if (update.getMessage().hasLocation())
                {
                    System.out.println("LOCATION");
                    database.saveLocationSentByUserInDatabase(update);
                }
                else
                {
                    if (update.getMessage().getVideo() != null)
                    {
                        System.out.println("VIDEO");
                        database.saveVideoSentByUserInDatabase(update);
                    }
                    else if (update.getMessage().getAudio() != null)
                    {
                        System.out.println("AUDIO");
                        database.saveAudioSentByUserInDatabase(update);
                    }
                    else if (update.getMessage().getVoice() != null)
                    {
                        System.out.println("VOICE");
                        database.saveVoiceSentByUserInDatabase(update);
                    }
                    else if (update.getMessage().getGame() != null)
                    {
                        System.out.println("GAME");
                        database.saveGameSentByUserInDatabase(update);
                    }
                }
            }
            else if (update.getMessage().hasText() && update.getMessage().getText().equals("/reg"))
            {
                String messageText = "Мой милый друг, приветствую тебя, регистрация прошла удачно! Чтобы получить полный список команд, отправь мне команду /help";
                database.registerUser(update);
                sendServiceMessageToUser(update, messageText);
            }
            else
            {
                String messageText = "Дорогой Гость, Вам надо зарегистрироваться! Для этого отправьте мне команду /reg";
                sendServiceMessageToUser(update, messageText);
            }
        }
        sendServiceMessageToUser(update, infDb.toString());
    }

    // Sending a service message to the user
    private void sendServiceMessageToUser(Update update, String messageText) {
        long chatId = update.getMessage().getChatId();

        SendMessage sendMessage = new SendMessage()
                .setChatId(chatId)
                .setText(messageText);

        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
