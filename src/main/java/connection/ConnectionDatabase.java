package connection;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.telegram.telegrambots.api.objects.Update;

import javax.sql.DataSource;
import java.sql.*;

public class ConnectionDatabase {

    private final String url = "jdbc:mysql://localhost:3306/telegrambot";
    private final String name = "root";
    private final String password = "root";

    private MysqlDataSource dataSource = null;
    private Connection connection = null;

    public ConnectionDatabase() {

    }

    private DataSource getDataSource() {
        dataSource = new MysqlDataSource();
        dataSource.setURL(url);
        dataSource.setUser(name);
        dataSource.setPassword(password);
        return dataSource;
    }

    // The connection to the database is set
    public Connection getConnectionDatabase() {
        getDataSource();
        try {
            connection = dataSource.getConnection();
            System.out.println("Database connected!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    // The connection to the database is closed
    public void closeConnection(Connection connection) {
        if (connection != null)
        {
            try {
                connection.close();
                System.out.println("Database disconnected!");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
