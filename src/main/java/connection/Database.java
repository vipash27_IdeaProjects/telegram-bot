package connection;

import bot.MachinariumBot;
import org.telegram.telegrambots.api.methods.GetFile;
import org.telegram.telegrambots.api.objects.File;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;

public class Database {

    private MachinariumBot bot = new MachinariumBot();
    private ConnectionDatabase connectionMysql = new ConnectionDatabase();
    private Connection connection;

    // User registration
    public void registerUser(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        int idUser = update.getMessage().getFrom().getId();
        String firstName = update.getMessage().getFrom().getFirstName();
        String lastName = update.getMessage().getFrom().getLastName();
        String userName = update.getMessage().getFrom().getUserName();
        int dateReg = update.getMessage().getDate();
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate("INSERT INTO users (id_user, firstName, lastName, userName, date_reg) values (" + idUser +", '" + firstName + "', '" + lastName + "', " +
                    "'" + userName + "', " + dateReg + ")");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.closeConnection(connection);
    }

    // Get the primary key id of the users table
    public int getPrimaryKeyIdUser(Update update, Connection connection) {
        int id = 0;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select id from users where id_user = " + update.getMessage().getChatId() + "");
            if (resultSet.next())
            {
                id = resultSet.getInt("id");
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    // The text sent by the user is stored in the database
    public void saveTextSentByUserInDatabase(Update update, String message) {
        connection = connectionMysql.getConnectionDatabase();
        int dateReg = update.getMessage().getDate();
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate("INSERT INTO historytext (id_user, message_out, date_message) values (" + getPrimaryKeyIdUser(update, connection) + ", '" + message + "', " + dateReg + ")");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.closeConnection(connection);
    }

    // The text received by the user is stored in the database
    public void saveTextSentToUser(Update update, String message) {
        connection = connectionMysql.getConnectionDatabase();
        int dateReg = update.getMessage().getDate();
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate("INSERT INTO historytext (id_user, message_in, date_message) values (" + getPrimaryKeyIdUser(update, connection) + ", '" + message + "', " + dateReg + ")");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.closeConnection(connection);
    }

    // Checking the user in the database
    public boolean checkUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        boolean flag = false;
        int getIdUser = update.getMessage().getFrom().getId();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery("SELECT id_user FROM users");
            while (rs.next())
            {
                int idUser = rs.getInt("id_user");
                if (idUser == getIdUser)
                {
                    flag = true;
                    break;
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.closeConnection(connection);
        return flag;
    }

    // The photo sent by the user is stored in the database
    public void savePhotoSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        GetFile getFile = new GetFile();
        getFile.setFileId(update.getMessage().getPhoto().get(0).getFileId());
        String sqlQuery = "INSERT INTO historyphoto (id_user, photo_out, date_message) values (?, ?, ?)";
        saveFileSentByUserInDatabase(update, getFile, connection, sqlQuery);
        connectionMysql.closeConnection(connection);
    }

    // The video sent by the user is stored in the database
    public void saveVideoSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        GetFile getFile = new GetFile();
        getFile.setFileId(update.getMessage().getVideo().getFileId());
        String sqlQuery = "INSERT INTO historyvideo (id_user, video_out, date_message) values (?, ?, ?)";
        saveFileSentByUserInDatabase(update, getFile, connection, sqlQuery);
        connectionMysql.closeConnection(connection);
    }

    // The audio sent by the user is stored in the database
    public void saveAudioSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        GetFile getFile = new GetFile();
        getFile.setFileId(update.getMessage().getAudio().getFileId());
        String sqlQuery = "INSERT INTO historyaudio (id_user, audio_out, date_message) values (?, ?, ?)";
        saveFileSentByUserInDatabase(update, getFile, connection, sqlQuery);
        connectionMysql.closeConnection(connection);
    }

    // The voice sent by the user is stored in the database
    public void saveVoiceSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        GetFile getFile = new GetFile();
        getFile.setFileId(update.getMessage().getVoice().getFileId());
        String sqlQuery = "INSERT INTO historyvoice (id_user, voice_out, date_message) values (?, ?, ?)";
        saveFileSentByUserInDatabase(update, getFile, connection, sqlQuery);
        connectionMysql.closeConnection(connection);
    }

    // The document sent by the user is stored in the database
    public void saveDocumentSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        GetFile getFile = new GetFile();
        getFile.setFileId(update.getMessage().getDocument().getFileId());
        String sqlQuery = "INSERT INTO historydocument (id_user, document_out, date_message) values (?, ?, ?)";
        saveFileSentByUserInDatabase(update, getFile, connection, sqlQuery);
        connectionMysql.closeConnection(connection);
    }

    // The game sent by the user is stored in the database
    public void saveGameSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        GetFile getFile = new GetFile();
        getFile.setFileId(update.getMessage().getGame().getAnimation().getFileId());
        String sqlQuery = "INSERT INTO historygame (id_user, game_out, date_message) values (?, ?, ?)";
        saveFileSentByUserInDatabase(update, getFile, connection, sqlQuery);
        connectionMysql.closeConnection(connection);
    }

    // The location sent by the user is stored in the database
    public void saveLocationSentByUserInDatabase(Update update) {
        connection = connectionMysql.getConnectionDatabase();
        Float lat = update.getMessage().getLocation().getLatitude();
        Float lng = update.getMessage().getLocation().getLongitude();

        String sqlQuery = "INSERT INTO historylocation (id_user, location_latitude_out, location_longitude_out, date_message)" +
                " values (?, ?, ?, ?)";

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, getPrimaryKeyIdUser(update, connection));
            preparedStatement.setFloat(2, lat);
            preparedStatement.setFloat(3, lng);
            preparedStatement.setInt(4, update.getMessage().getDate());
            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        connectionMysql.closeConnection(connection);
    }

    // The file sent by the user is stored in the database (the tables are historytext, historyphoto
    // historyvideo, historyvoice, historyaudio, historydocument, historygame, historylocation)
    private void saveFileSentByUserInDatabase(Update update, GetFile getFile, Connection connection, String sqlQuery) {
        PreparedStatement preparedStatement = null;
        try {
            File file = bot.getFile(getFile);
            URL url = new URL("https://api.telegram.org/file/bot" + bot.getBotToken() + "/" + file.getFilePath() + "");

            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, getPrimaryKeyIdUser(update, connection));
            preparedStatement.setURL(2, url);
            preparedStatement.setInt(3, update.getMessage().getDate());
            preparedStatement.executeUpdate();

            preparedStatement.close();

        } catch (TelegramApiException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}