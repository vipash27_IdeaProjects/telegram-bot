package connection;

import org.telegram.telegrambots.api.objects.Update;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InformationDataUser {

    private ConnectionDatabase connectionDatabase = new ConnectionDatabase();
    private Connection connection = null;
    private Database database = new Database();
    private ArrayList<String> listFiles = new ArrayList<String>();

    private Update update;

    public InformationDataUser(Update update) {
        this.update = update;
    }

    private int getCountTextMessage() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(message_out) FROM historytext " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountVoice() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(voice_out) FROM historyvoice " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountAudio() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(audio_out) FROM historyaudio " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountGame() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(game_out) FROM historygame " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountDocument() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(document_out) FROM historydocument " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountVideo() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(video_out) FROM historyvideo " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountPhoto() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(photo_out) FROM historyphoto " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCountLocation() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT COUNT(location_latitude_out) FROM historylocation " +
                "WHERE id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        return getCount(sqlQuery);
    }

    private int getCount(String sqlQuery) {
        Statement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next())
            {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (resultSet != null)
                {
                    resultSet.close();
                }
                if (statement != null)
                {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (connection != null)
            {
                connectionDatabase.closeConnection(connection);
            }
        }
        return count;
    }

    private int getTotalCountFiles() {
        return getCountVoice() + getCountAudio() + getCountGame() + getCountDocument() + getCountVideo() + getCountPhoto() + getCountLocation();
    }

    private int getFilesExtensionNotFound() {
        return getCountGame() + getCountLocation() + getCountVideo() + getCountVoice();
    }

    private void getFullDataAudio() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT audio_out FROM historyaudio where id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        getListFilePath(connection, sqlQuery);
    }

    private void getFullDataDocument() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT document_out FROM historydocument where id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        getListFilePath(connection, sqlQuery);
    }

    private void getFullDataPhoto() {
        connection = connectionDatabase.getConnectionDatabase();
        String sqlQuery = "SELECT photo_out FROM historyphoto where id_user = " + database.getPrimaryKeyIdUser(update, connection) + "";
        getListFilePath(connection, sqlQuery);
    }

    private void getListFilePath(Connection connection, String sqlQuery) {

        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next())
            {
                listFiles.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (resultSet != null)
                {
                    resultSet.close();
                }
                if (statement != null)
                {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (connection != null)
            {
                connectionDatabase.closeConnection(connection);
            }
        }
    }

    private void getTypeExtension() {
        getFullDataAudio();
        getFullDataDocument();
        getFullDataPhoto();
        for (int i = 0; i < listFiles.size(); i++)
        {
            int index = 0;
            char[] ch = listFiles.get(i).toCharArray();
            for (int j = ch.length - 1; j >= 0; j--)
            {
                if (ch[j] == '.')
                {
                    index = j;
                    break;
                }
            }
            listFiles.set(i, listFiles.get(i).substring(index));
        }
    }

    private String getCountTypeExtension() {
        getTypeExtension();
        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        int index = 0;
        for (int i = 0; i < listFiles.size(); i++)
        {
            for (int j = 0; j < listFiles.size(); j++)
            {
                if (listFiles.get(i).equals(listFiles.get(j)))
                {
                    index++;
                }
            }
            hashMap.put(listFiles.get(i), index);
            index = 0;
        }
        String line = "";
        for (Map.Entry<String, Integer> str : hashMap.entrySet())
        {
            line = line + str.getKey() + " - " + str.getValue() + "; ";
        }
        return line;
    }
//======================================================================================================================
    @Override
    public String toString() {
        String line = "TEXT MESSAGES: " + getCountTextMessage() + "\nTOTAL NUMBER OF FILES: " + getTotalCountFiles() + "\n" +
                "       of them: \n" + getCountTypeExtension() + "\n       file extension not found: " + getFilesExtensionNotFound();
        return line;
    }
}
