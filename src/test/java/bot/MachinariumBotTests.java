package bot;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Vitali on 24.10.2017.
 */
public class MachinariumBotTests {

    MachinariumBot machinarium = new MachinariumBot();

    @Test
    public void getBotUsernameTest() {
        assertEquals(machinarium.getBotUsername(), "MachinariumSuperBot");
    }

    @Test
    public void getBotTokenTest() {
        assertEquals(machinarium.getBotToken(), "479400190:AAFSoZGbcaNrvtNLUCnWJfJsGRQRqDlVm-I");
    }

}
