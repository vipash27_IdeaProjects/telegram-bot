import connection.ConnectionDatabase;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class MuTestTest {

    @Test
    public void aPlusB() {
        MuTest muTest = new MuTest();
        int x = muTest.getSumma(2, 3);
        assertEquals(x, 5);
    }
    /*
    @Test
    public void methodExist() {
        MuTest muTest = new MuTest();
        int x = muTest.multiplication(2, 2);
    }


    @Test
    public void checkTwoMultiplicationTwo() {
        MuTest muTest = new MuTest();
        int x = muTest.multiplication(2, 2);
        assertEquals(x, 4);
    }

    @Test
    public void checkThreeMultiplicationFive() {
        MuTest muTest = new MuTest();
        int x = muTest.multiplication(3, 5);
        assertEquals(x, 15);
    }*/

    /*@Test
    public void methodExist() {
        MuTest muTest = new MuTest();
        muTest.blal();
    }

    @Test
    public void defaultValueOfX() {
        MuTest muTest = new MuTest();
        assertEquals(muTest.getX(), 0);
    }

    @Test
    public void checkTwoMultiplicationFive() {
        MuTest muTest = new MuTest();
        muTest.blal();
        assertEquals(muTest.getX(), 6);
    }

    /*@Test
    public void checkTwoSummaThree() {
        MuTest muTest = new MuTest();
        assertEquals(muTest.getSumma(2, 3), 5);
    }

    @Test
    public void checkTwoSummaFive() {
        MuTest muTest = new MuTest();
        assertEquals(muTest.getSumma(2, 5), 7);
    }

    @Test
    public void maxSumm() {
        MuTest muTest = new MuTest();
        assertEquals(muTest.getSumma(Integer.MAX_VALUE, 7), Integer.MAX_VALUE);
    }

    @Test
    public void maxSumm2() {
        MuTest muTest = new MuTest();
        assertEquals(muTest.getSumma(7, Integer.MAX_VALUE), Integer.MAX_VALUE);
    }

    @Test
    public void maxSumm3() {
        MuTest muTest = new MuTest();
        assertEquals(muTest.getSumma( - 100, - 900), Integer.MAX_VALUE);
    }
//======================================================================================================================
    /*@Test
    public void existMethodGetConnectionDatabase() {
        ConnectionDatabase connDat = new ConnectionDatabase();
        connDat.getConnectionDatabase();
    }

    @Test
    public void checkConnectedWithDatabase() {
        ConnectionDatabase connDat = new ConnectionDatabase();
        assertNotNull(connDat.getConnectionDatabase());
    }

    @Test
    public void checkNameDatabase() {
        ConnectionDatabase connDat = new ConnectionDatabase();
        try {
            assertEquals(connDat.getConnectionDatabase().getCatalog(), "telegrambot");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/
}